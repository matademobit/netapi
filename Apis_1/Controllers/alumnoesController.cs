﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Apis_1;

namespace Apis_1.Controllers
{
    public class alumnoesController : ApiController
    {
        private test1Entities db = new test1Entities();

        public alumnoesController()
        {
            db.Configuration.ProxyCreationEnabled = false;

        }


        // GET: api/alumnoes
        public IQueryable<alumno> Getalumno()
        {
            return db.alumno;
        }

        // GET: api/alumnoes/5
        [ResponseType(typeof(alumno))]
        public IHttpActionResult Getalumno(int id)
        {
            alumno alumno = db.alumno.Find(id);
            if (alumno == null)
            {
                return NotFound();
            }

            return Ok(alumno);
        }

        // PUT: api/alumnoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putalumno(int id, alumno alumno)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != alumno.id)
            {
                return BadRequest();
            }

            db.Entry(alumno).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!alumnoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/alumnoes
        [ResponseType(typeof(alumno))]
        public IHttpActionResult Postalumno(alumno alumno)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.alumno.Add(alumno);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = alumno.id }, alumno);
        }

        // DELETE: api/alumnoes/5
        [ResponseType(typeof(alumno))]
        public IHttpActionResult Deletealumno(int id)
        {
            alumno alumno = db.alumno.Find(id);
            if (alumno == null)
            {
                return NotFound();
            }

            db.alumno.Remove(alumno);
            db.SaveChanges();

            return Ok(alumno);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool alumnoExists(int id)
        {
            return db.alumno.Count(e => e.id == id) > 0;
        }
    }
}